module RedmineProjectTeam
  module Hooks
    class ViewsIssuesHook < Redmine::Hook::ViewListener
      render_on :view_issues_sidebar_issues_bottom,
        partial: 'issues/team_sidebar'
    end
  end
end
