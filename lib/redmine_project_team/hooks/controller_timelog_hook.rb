module RedmineProjectTeam
  module Hooks
    class ControllerTimelogHook < Redmine::Hook::ViewListener

      def controller_timelog_edit_before_save(context = {})
      	params = context[:params]
        time_entry = context[:time_entry]
        if User.current.allowed_to?(:log_time_for_users, time_entry.project) 
          if params[:time_entry].key?(:user_id)
            user = User.find(params[:time_entry][:user_id])
         	else
            user = User.current
          end
          time_entry.user = user
        end
      end

    end
  end
end