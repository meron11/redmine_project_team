module RedmineProjectTeam
  module Hooks
    class ViewsProjectsHook < Redmine::Hook::ViewListener
      render_on :view_projects_show_left, partial: 'projects/team_assign'
      render_on :view_projects_show_right, partial: 'projects/team_spent_time'
    end
  end
end
