module TimelogTeamHelper

  def user_collection_for_select_options( time_entry)
    project ||= time_entry.try(:project)
    project ||= @project
    
    users = project.members.map{ |member| member.user}
   
    users.keep_if{ |user| user.allowed_to?(:log_time, project)}
    collection = []
    users.each { |user| collection << [user.name, user.id] }
    collection
  end
  
end