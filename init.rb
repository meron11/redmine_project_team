Redmine::Plugin.register :redmine_project_team do
  name 'Redmine Project Team Plugin'
  author 'Emmanuel Auvinet'
  description 'This plugin adds funtionnalities to help to manage the team of the project'
  version '0.0.2'
  url 'https://bitbucket.org/eauvinet/redmine_project_team'
  
  project_module :time_tracking do
    permission :log_time_for_users, {:timelog => [:new, :create]}, :require => :loggedin
  end
end


ActionDispatch::Callbacks.to_prepare do

	ProjectsController.send :helper, 'project_team'
  TimelogController.send :helper, 'timelog_team'
  
  unless IssuesHelper.included_modules.include? RedmineProjectTeam::IssuesHelperPatch
    IssuesHelper.send(:include, RedmineProjectTeam::IssuesHelperPatch)
  end
end

require 'redmine_project_team/hooks/views_issues_hook'
require 'redmine_project_team/hooks/views_projects_hook'
require 'redmine_project_team/hooks/views_timelog_hook'
require 'redmine_project_team//hooks/controller_timelog_hook'

